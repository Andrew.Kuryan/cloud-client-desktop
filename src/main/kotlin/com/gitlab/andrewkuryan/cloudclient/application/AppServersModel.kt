package com.gitlab.andrewkuryan.cloudclient.application

import com.gitlab.andrewkuryan.cloudclient.application.port.CloudApp
import com.gitlab.andrewkuryan.cloudclient.application.port.ServersModel
import com.gitlab.andrewkuryan.cloudclient.entity.AbstractCredentials
import com.gitlab.andrewkuryan.cloudclient.entity.AbstractUser
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch

class AppServersModel(
    private val app: CloudApp
) : ServersModel {

    /*override val servers = arrayListOf<SmbServer>()

    override var chosenServer: SmbServer? = null*/

    override var activeUser: AbstractUser? = null

    private val ioScope = CoroutineScope(Dispatchers.IO)
    private val uiScope = CoroutineScope(Dispatchers.Main)

    private val pathSpliterator = "/"

    /*override fun connect(host: String, callback: () -> Unit) {
        ioScope.launch {
            val result = ioScope.async {
                app.api.connect(host)
            }.await()
            if (result.isSuccess) {
                uiScope.launch {
                    val server = SmbServer(result.getOrNull()!!, pathSpliterator)
                    *//*servers.add(server)
                    chosenServer = server*//*
                    callback()
                }
            } else {
                result.exceptionOrNull()!!.printStackTrace()
            }
        }
    }*/

    override fun <T: AbstractCredentials> connectWithCredentials(
        credentials: T,
        callback: () -> Unit
    ) {
        ioScope.launch {
            val result = ioScope.async {
                app.api.connectWithCredentials(credentials)
            }.await()
            if (result.isSuccess) {
                uiScope.launch {
                    activeUser = result.getOrNull()!!
                    // val server = SmbServer(result.getOrNull()!!, pathSpliterator)
                    /*servers.add(server)
                    chosenServer = server*/
                    callback()
                }
            }
        }
    }
}