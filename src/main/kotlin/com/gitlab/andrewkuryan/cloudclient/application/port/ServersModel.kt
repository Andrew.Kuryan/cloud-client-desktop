package com.gitlab.andrewkuryan.cloudclient.application.port

import com.gitlab.andrewkuryan.cloudclient.entity.AbstractCredentials
import com.gitlab.andrewkuryan.cloudclient.entity.AbstractUser

interface ServersModel {

    /*val servers: ArrayList<SmbServer>
    var chosenServer: SmbServer?*/

    // fun connect(host: String, callback: () -> Unit)
    var activeUser: AbstractUser?

    fun <T: AbstractCredentials> connectWithCredentials(credentials: T, callback: () -> Unit)
}