package com.gitlab.andrewkuryan.cloudclient.application

import com.gitlab.andrewkuryan.cloudclient.application.port.CloudApp
import com.gitlab.andrewkuryan.cloudclient.dataprovider.AbstractFileApi
import com.gitlab.andrewkuryan.cloudclient.dataprovider.smd.SMBFileApi
import com.gitlab.andrewkuryan.cloudclient.entity.AbstractCredentials

class CloudClientApplication(
        private val navigateToImage: CloudApp.() -> Unit,
        private val navigateToFiles: CloudApp.() -> Unit,
        private val navigateToUsers: CloudApp.() -> Unit,
        private val navigateToUpload: CloudApp.() -> Unit
) : CloudApp {

    override val api: AbstractFileApi<in AbstractCredentials> = SMBFileApi() as AbstractFileApi<in AbstractCredentials>

    override val files = AppFilesModel(this)
    override val servers = AppServersModel(this)
    override val image = AppImageModel(this)
    override val upload = AppUploadModel(this)

    override fun navigateTo(path: String) {
        when (path) {
            "image" -> navigateToImage()
            "files" -> navigateToFiles()
            "users" -> navigateToUsers()
            "upload" -> navigateToUpload()
        }
    }
}