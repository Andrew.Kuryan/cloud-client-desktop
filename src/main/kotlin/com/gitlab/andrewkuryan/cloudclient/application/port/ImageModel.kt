package com.gitlab.andrewkuryan.cloudclient.application.port

import com.gitlab.andrewkuryan.cloudclient.entity.resource.ImageResource

interface ImageModel {

    val loadedResources: Array<ByteArray>
    var availableResources: List<ImageResource>
    var currentResourcePos: Int
    var isLoading: Boolean

    fun setIsLoadingListener(listener: (Boolean) -> Unit)
    fun close()

    fun getCurrentImageContent(callback: () -> Unit)
    fun refresh(callback: () -> Unit)
    fun moveNext()
    fun movePrev()
}