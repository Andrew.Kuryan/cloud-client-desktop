package com.gitlab.andrewkuryan.cloudclient.application.port

import com.gitlab.andrewkuryan.cloudclient.entity.resource.AbstractResource

interface FilesModel {

    var resources: ArrayList<AbstractResource>
    var currentPath: String
    var isLoading: Boolean

    fun setIsLoadingListener(listener: (Boolean) -> Unit)
    fun setOnCurrentPathChangeListener(listener: (String) -> Unit)
    fun setOnResourcesChangeListener(listener: (ArrayList<AbstractResource>) -> Unit)

    fun refresh()
    fun onResourceClick(resource: AbstractResource)
    fun navigateBack()
    fun navigateTo(path: String)
    fun onAddFileClick()
}