package com.gitlab.andrewkuryan.cloudclient.application

import com.gitlab.andrewkuryan.cloudclient.application.port.CloudApp
import com.gitlab.andrewkuryan.cloudclient.application.port.ImageModel
import com.gitlab.andrewkuryan.cloudclient.entity.resource.ImageResource
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class AppImageModel(
    private val app: CloudApp
): ImageModel {

    override var loadedResources = arrayOf<ByteArray>()
    override var availableResources: List<ImageResource> = arrayListOf()
        set(value) {
            loadedResources = Array(value.size) { ByteArray(0) }
            currentResourcePos = 0
            field = value
        }
    override var currentResourcePos = 0

    private val ioScope = CoroutineScope(Dispatchers.IO)
    private val uiScope = CoroutineScope(Dispatchers.Main)

    override var isLoading = false
        set(value) {
            if (isLoadingListener != null) {
                isLoadingListener!!(value)
            }
            field = value
        }
    private var isLoadingListener: ((Boolean) -> Unit)? = null

    override fun setIsLoadingListener(listener: (Boolean) -> Unit) {
        isLoadingListener = listener
    }

    override fun close() {
        app.navigateTo("files")
    }

    override fun getCurrentImageContent(callback: () -> Unit) {
        println(currentResourcePos)
        isLoading = true
        if (loadedResources[currentResourcePos].isNotEmpty()) {
            callback()
            isLoading = false
        } else {
            loadedResources[currentResourcePos] = ByteArray(0)
            ioScope.launch {
                val result = app.api.getResourceBytes(
                    app.servers.activeUser!!,
                    availableResources[currentResourcePos]
                )
                if (result.isSuccess) {
                    uiScope.launch {
                        loadedResources[currentResourcePos] = result.getOrNull()!!
                        callback()
                        isLoading = false
                    }
                } else {
                    isLoading = false
                }
            }
        }
    }

    override fun refresh(callback: () -> Unit) {
        isLoading = true
        loadedResources[currentResourcePos] = ByteArray(0)
        ioScope.launch {
            val result = app.api.getResourceBytes(
                app.servers.activeUser!!,
                availableResources[currentResourcePos]
            )
            if (result.isSuccess) {
                uiScope.launch {
                    loadedResources[currentResourcePos] = result.getOrNull()!!
                    callback()
                    isLoading = false
                }
            } else {
                isLoading = false
            }
        }
    }

    override fun moveNext() {
        if (currentResourcePos + 1 <= availableResources.size - 1) {
            currentResourcePos++
        } else {
            currentResourcePos = 0
        }
    }

    override fun movePrev() {
        if (currentResourcePos - 1 >= 0) {
            currentResourcePos--
        } else {
            currentResourcePos = availableResources.size - 1
        }
    }
}
