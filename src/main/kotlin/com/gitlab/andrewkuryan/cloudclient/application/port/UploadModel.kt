package com.gitlab.andrewkuryan.cloudclient.application.port

interface UploadModel {

    var isLoading: Boolean
    var devicePath: String?
    var serverName: String?

    fun setIsLoadingListener(listener: (Boolean) -> Unit)

    fun upload()
    fun close()
}