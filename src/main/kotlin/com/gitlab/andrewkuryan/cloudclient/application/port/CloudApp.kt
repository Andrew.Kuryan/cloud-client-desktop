package com.gitlab.andrewkuryan.cloudclient.application.port

import com.gitlab.andrewkuryan.cloudclient.dataprovider.AbstractFileApi
import com.gitlab.andrewkuryan.cloudclient.entity.AbstractCredentials

interface CloudApp {

    val api: AbstractFileApi<in AbstractCredentials>

    val files: FilesModel
    val servers: ServersModel
    val image: ImageModel
    val upload: UploadModel

    fun navigateTo(path: String)
}