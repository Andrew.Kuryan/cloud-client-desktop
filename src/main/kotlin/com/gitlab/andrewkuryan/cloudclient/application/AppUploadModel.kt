package com.gitlab.andrewkuryan.cloudclient.application

import com.gitlab.andrewkuryan.cloudclient.application.port.CloudApp
import com.gitlab.andrewkuryan.cloudclient.application.port.UploadModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.nio.file.Files
import java.nio.file.Paths

class AppUploadModel(
        private val app: CloudApp
) : UploadModel {

    override var devicePath: String? = null
    override var serverName: String? = null

    override var isLoading = false
        set(value) {
            if (isLoadingListener != null) {
                isLoadingListener!!(value)
            }
            field = value
        }
    private var isLoadingListener: ((Boolean) -> Unit)? = null

    private val ioScope = CoroutineScope(Dispatchers.IO)
    private val uiScope = CoroutineScope(Dispatchers.IO)

    override fun setIsLoadingListener(listener: (Boolean) -> Unit) {
        isLoadingListener = listener
    }

    override fun upload() {
        if (devicePath != null && serverName != null && app.servers.activeUser != null) {
            isLoading = true
            ioScope.launch {
                try {
                    val data = withContext(Dispatchers.IO) {
                        Files.readAllBytes(Paths.get(devicePath!!))
                    }
                    val result = app.api.uploadFile(
                            app.servers.activeUser!!,
                            app.files.currentPath + "/" + serverName!!,
                            data
                    )
                    if (result != null) throw result
                    uiScope.launch {
                        isLoading = false
                    }
                    app.files.refresh()
                } catch (exc: Exception) {
                    uiScope.launch {
                        isLoading = false
                    }
                }
            }
        }
    }

    override fun close() {
        app.navigateTo("files")
    }
}