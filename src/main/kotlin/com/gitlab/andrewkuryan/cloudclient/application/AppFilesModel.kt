package com.gitlab.andrewkuryan.cloudclient.application

import com.gitlab.andrewkuryan.cloudclient.application.port.CloudApp
import com.gitlab.andrewkuryan.cloudclient.application.port.FilesModel
import com.gitlab.andrewkuryan.cloudclient.entity.resource.AbstractResource
import com.gitlab.andrewkuryan.cloudclient.entity.resource.DirectoryResource
import com.gitlab.andrewkuryan.cloudclient.entity.resource.FileResource
import com.gitlab.andrewkuryan.cloudclient.entity.resource.ImageResource
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class AppFilesModel(
        private val app: CloudApp
) : FilesModel {

    private val pathSpliterator = "/"
    private val ioScope = CoroutineScope(Dispatchers.IO)
    private val uiScope = CoroutineScope(Dispatchers.Main)

    override var resources = arrayListOf<AbstractResource>()
    private var onResourcesChangeListener: ((ArrayList<AbstractResource>) -> Unit)? = null

    override var currentPath = ""
        set(value) {
            if (onCurrentPathChangeListener != null) {
                onCurrentPathChangeListener!!(value)
            }
            field = value
        }
    private var onCurrentPathChangeListener: ((String) -> Unit)? = null

    override var isLoading = false
        set(value) {
            if (isLoadingListener != null) {
                isLoadingListener!!(value)
            }
            field = value
        }
    private var isLoadingListener: ((Boolean) -> Unit)? = null

    override fun setIsLoadingListener(listener: (Boolean) -> Unit) {
        isLoadingListener = listener
    }

    override fun setOnCurrentPathChangeListener(listener: (String) -> Unit) {
        onCurrentPathChangeListener = listener
    }

    override fun setOnResourcesChangeListener(listener: (ArrayList<AbstractResource>) -> Unit) {
        onResourcesChangeListener = listener
    }

    private fun clearResources() {
        resources.clear()
        if (onResourcesChangeListener != null) {
            onResourcesChangeListener!!(resources)
        }
    }

    private fun addToResources(data: List<AbstractResource>) {
        val grouped = groupResources(data)
        resources.addAll(grouped)
        if (onResourcesChangeListener != null) {
            onResourcesChangeListener!!(resources)
        }
    }

    private fun groupResources(data: List<AbstractResource>): List<AbstractResource> {
        val folders = data.filterIsInstance<DirectoryResource>()
        val otherRes = data.filter { it !is DirectoryResource }
        return folders + otherRes
    }

    private fun navigate(newPath: String) {
        if (app.servers.activeUser != null) {
            isLoading = true
            clearResources()
            currentPath = newPath
            ioScope.launch {
                val result = app.api.getDirectoryContent(
                        app.servers.activeUser!!,
                        currentPath,
                        pathSpliterator
                )
                if (result.isSuccess) {
                    uiScope.launch {
                        addToResources(result.getOrNull() ?: emptyList())
                        isLoading = false
                    }
                } else {
                    uiScope.launch {
                        isLoading = false
                    }
                }
            }
        }
    }

    override fun navigateTo(path: String) {
        println("Navigate to: $path")
        navigate(path)
    }

    override fun navigateBack() {
        val previousPath = currentPath
                .split(pathSpliterator)
                .dropLast(1)
                .joinToString(pathSpliterator)
        println("Navigate Back: $previousPath")
        navigate(previousPath)
    }

    override fun refresh() {
        navigate(currentPath)
    }

    override fun onResourceClick(resource: AbstractResource) {
        if (app.servers.activeUser != null) {
            if (resource is DirectoryResource) {
                navigate(currentPath + "${pathSpliterator}${resource.name}")
            } else if (resource is FileResource) {
                if (resource is ImageResource) {
                    val imageResources = resources.filterIsInstance<ImageResource>()
                    val currentPos = imageResources.indexOfFirst { it.path == resource.path }
                    app.image.availableResources = imageResources
                    app.image.currentResourcePos = currentPos
                    app.navigateTo("image")
                }
            }
        }
    }

    override fun onAddFileClick() {
        app.navigateTo("upload")
    }
}