package com.gitlab.andrewkuryan.cloudclient.dataprovider

import com.gitlab.andrewkuryan.cloudclient.entity.AbstractCredentials
import com.gitlab.andrewkuryan.cloudclient.entity.AbstractUser
import com.gitlab.andrewkuryan.cloudclient.entity.resource.AbstractResource

interface AbstractFileApi<C: AbstractCredentials> {

    fun connectWithCredentials(credentials: C): Result<AbstractUser>

    fun disconnect(user: AbstractUser): Boolean

    fun getResource(user: AbstractUser, path: String, pathSpliterator: String): Result<AbstractResource>

    fun getDirectoryContent(user: AbstractUser, path: String, pathSpliterator: String): Result<List<AbstractResource>>

    fun getResourceBytes(user: AbstractUser, resource: AbstractResource): Result<ByteArray>

    fun uploadFile(user: AbstractUser, path: String, data: ByteArray): Exception?
}