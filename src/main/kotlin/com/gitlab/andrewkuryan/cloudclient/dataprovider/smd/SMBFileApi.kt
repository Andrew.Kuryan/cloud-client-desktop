package com.gitlab.andrewkuryan.cloudclient.dataprovider.smd

import com.gitlab.andrewkuryan.cloudclient.dataprovider.AbstractFileApi
import com.gitlab.andrewkuryan.cloudclient.dataprovider.ResourceNotFoundException
import com.gitlab.andrewkuryan.cloudclient.dataprovider.UnathorizedException
import com.gitlab.andrewkuryan.cloudclient.entity.AbstractUser
import com.gitlab.andrewkuryan.cloudclient.entity.resource.*
import com.gitlab.andrewkuryan.cloudclient.entity.smb.SimpleSmbCredentials
import com.gitlab.andrewkuryan.cloudclient.entity.smb.SmbCredentials
import com.gitlab.andrewkuryan.cloudclient.entity.smb.SmbUser
import jcifs.CIFSContext
import jcifs.config.PropertyConfiguration
import jcifs.context.BaseContext
import jcifs.smb.NtlmPasswordAuthenticator
import jcifs.smb.SmbException
import jcifs.smb.SmbFile
import java.util.*

class SMBFileApi : AbstractFileApi<SimpleSmbCredentials> {

    private val properties = Properties()
    private val contexts = mutableMapOf<String, Context>()

    private data class Context(
            val host: String,
            val context: CIFSContext
    )

    fun connect(host: String): Result<SmbUser> {
        val context = BaseContext(PropertyConfiguration(properties))
        return try {
            val file = SmbFile("smb://$host", context)
            file.length()
            val user = SmbUser.guest()
            contexts[user.uid] = Context(host, context)
            Result.success(user)
        } catch (exc: SmbException) {
            Result.failure(UnathorizedException())
        }
    }

    override fun connectWithCredentials(
            credentials: SimpleSmbCredentials
    ): Result<SmbUser> {
        if (credentials is SmbCredentials) {
            val context = BaseContext(PropertyConfiguration(properties))
            val contextWithCred = context.withCredentials(
                    NtlmPasswordAuthenticator(
                            credentials.domain,
                            credentials.username,
                            credentials.password
                    )
            )
            val file = SmbFile("smb://${credentials.host}", contextWithCred)
            return try {
                file.length()
                val user = SmbUser(credentials.username, credentials.password)
                contexts[user.uid] = Context(credentials.host, contextWithCred)
                Result.success(user)
            } catch (exc: SmbException) {
                Result.failure(UnathorizedException())
            }
        } else return connect(credentials.host)
    }

    override fun disconnect(user: AbstractUser): Boolean {
        contexts[user.uid] ?: return false
        contexts.remove(user.uid)
        return true
    }

    override fun getResource(user: AbstractUser, path: String, pathSpliterator: String): Result<AbstractResource> {
        val context = contexts[user.uid] ?: return Result.failure(UnathorizedException())
        val smbResource = SmbFile("smb://${context.host}$path", context.context)
        return try {
            smbResource.length()
            val resource = smbResource.toResource(path, pathSpliterator)
            Result.success(resource)
        } catch (exc: SmbException) {
            Result.failure(ResourceNotFoundException())
        }
    }

    override fun getDirectoryContent(
            user: AbstractUser,
            path: String,
            pathSpliterator: String
    ): Result<List<AbstractResource>> {
        val context = contexts[user.uid] ?: return Result.failure(UnathorizedException())
        val smbResource = SmbFile("smb://${context.host}$path", context.context)
        return try {
            smbResource.length()
            when {
                smbResource.isDirectory -> Result.success(smbResource.listFiles().map {
                    it.toResource("$path$pathSpliterator${it.simpleName(pathSpliterator)}", pathSpliterator)
                })
                else -> Result.failure(ResourceNotFoundException())
            }
        } catch (exc: SmbException) {
            Result.failure(ResourceNotFoundException())
        }
    }

    override fun getResourceBytes(user: AbstractUser, resource: AbstractResource): Result<ByteArray> {
        val context = contexts[user.uid] ?: return Result.failure(UnathorizedException())
        val smbResource = SmbFile("smb://${context.host}${resource.path}", context.context)
        return try {
            smbResource.length()
            Result.success(smbResource.openInputStream().readBytes())
        } catch (exc: SmbException) {
            Result.failure(ResourceNotFoundException())
        }
    }

    override fun uploadFile(user: AbstractUser, path: String, data: ByteArray): Exception? {
        val context = contexts[user.uid] ?: return UnathorizedException()
        val newFile = SmbFile("smb://${context.host}${path}", context.context)
        return try {
            newFile.openOutputStream().write(data)
            null
        } catch (exc: Exception) {
            exc
        }
    }

    private fun SmbFile.toResource(path: String, pathSpliterator: String): AbstractResource {
        return when {
            isFile -> {
                val file = FileResource(simpleName(pathSpliterator), path, Date(lastModified), Date(lastAccess()))
                if (file.extension in imageExtensions) {
                    ImageResource(file)
                } else file
            }
            isDirectory -> DirectoryResource(simpleName(pathSpliterator), path)
            else -> UnknownResource(simpleName(pathSpliterator), path)
        }
    }

    private fun SmbFile.simpleName(pathSpliterator: String): String {
        return when {
            isDirectory -> path.dropLast(1).split(pathSpliterator).last()
            else -> path.split(pathSpliterator).last()
        }
    }
}