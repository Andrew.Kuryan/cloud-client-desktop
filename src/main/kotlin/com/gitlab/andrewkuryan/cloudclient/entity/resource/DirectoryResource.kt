package com.gitlab.andrewkuryan.cloudclient.entity.resource

class DirectoryResource(
    name: String,
    path: String
) : AbstractResource(name, path) {

    val files: List<AbstractResource> = arrayListOf()

    override fun toString() = "Directory(name=$name)"
}