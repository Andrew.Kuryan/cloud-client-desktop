package com.gitlab.andrewkuryan.cloudclient.entity.smb

import com.gitlab.andrewkuryan.cloudclient.entity.AbstractCredentials

open class SimpleSmbCredentials(
    val host: String
) : AbstractCredentials()

class SmbCredentials(
    host: String,
    val domain: String,
    val username: String,
    val password: String
) : SimpleSmbCredentials(host)