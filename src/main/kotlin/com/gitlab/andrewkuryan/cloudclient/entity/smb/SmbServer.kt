package com.gitlab.andrewkuryan.cloudclient.entity.smb

import com.gitlab.andrewkuryan.cloudclient.entity.FileServer

class SmbServer(
    override val user: SmbUser,
    val pathSpliterator: String
): FileServer(user)