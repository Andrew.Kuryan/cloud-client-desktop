package com.gitlab.andrewkuryan.cloudclient.entity.resource

import java.util.*

class ImageResource(
    name: String,
    path: String,
    lastModifiedDate: Date,
    lastAccessDate: Date
): FileResource(name, path, lastModifiedDate, lastAccessDate) {

    constructor(fileResource: FileResource):
            this(fileResource.name, fileResource.path,
                fileResource.lastModifiedDate, fileResource.lastAccessDate)
}