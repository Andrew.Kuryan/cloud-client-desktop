package com.gitlab.andrewkuryan.cloudclient.entity.shortid

import java.util.*

object ShortId {

    private const val ORIGINAL = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_-"

    fun generate(): String {
        val random = Random()
        val str = StringBuilder("")
        repeat(8) {
            str.append(ORIGINAL[random.nextInt(ORIGINAL.length-1)])
        }
        return str.toString()
    }
}