package com.gitlab.andrewkuryan.cloudclient.entity.smb

import com.gitlab.andrewkuryan.cloudclient.entity.AbstractUser

class SmbUser(
    login: String,
    password: String
): AbstractUser(login) {

    companion object {

        fun guest() = SmbUser("Guest", "Guest")
    }
}