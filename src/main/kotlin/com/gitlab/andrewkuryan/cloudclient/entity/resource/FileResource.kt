package com.gitlab.andrewkuryan.cloudclient.entity.resource

import java.util.*

val imageExtensions = arrayListOf("png", "jpg", "jpeg")

open class FileResource(
    name: String,
    path: String,
    val lastModifiedDate: Date,
    val lastAccessDate: Date
): AbstractResource(name, path) {

    override fun toString() = "File(name=$name)"
}