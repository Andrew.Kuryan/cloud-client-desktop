package com.gitlab.andrewkuryan.cloudclient.entity.resource

class UnknownResource(
    name: String,
    path: String
) : AbstractResource(name, path)