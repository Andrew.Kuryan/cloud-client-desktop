package com.gitlab.andrewkuryan.cloudclient.entity.resource

abstract class AbstractResource(
    val name: String,
    val path: String
) {
    val extension: String
        get() = name.split(".").drop(1).joinToString(".")

    override fun toString() = "Resource(name=$name)"
}