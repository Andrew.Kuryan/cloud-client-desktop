package com.gitlab.andrewkuryan.cloudclient.entity

import com.gitlab.andrewkuryan.cloudclient.entity.shortid.ShortId

abstract class AbstractUser(
    val login: String
) {

    val uid: String = ShortId.generate()
}