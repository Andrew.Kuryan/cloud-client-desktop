package com.gitlab.andrewkuryan.cloudclient.ui

import javafx.geometry.Pos
import tornadofx.Stylesheet
import tornadofx.cssclass
import tornadofx.px

class FilesStyles : Stylesheet() {

    companion object {
        val filesRoot by cssclass()
        val filesList by cssclass()
        val plusButton by cssclass()

        val filesItem by cssclass()
        val filesContent by cssclass()

        val directoryItem by cssclass()
        val directoryContent by cssclass()
    }

    init {
        filesRoot {
            prefWidth = MainStyles.winWidth.px
            prefHeight = MainStyles.winHeight.px

            filesList {
                prefWidth = MainStyles.winWidth.px
                prefHeight = (MainStyles.winHeight - MainStyles.headerHeight).px
            }

            plusButton {
                translateX = (MainStyles.winWidth / 2 - 60).px
                translateY = (MainStyles.winHeight / 2 - 70).px
            }
        }

        filesItem {
            prefHeight = 30.px

            filesContent {
                alignment = Pos.CENTER_LEFT
                spacing = 10.px
            }
        }
        directoryItem {
            prefHeight = 30.px

            directoryContent {
                alignment = Pos.CENTER_LEFT
                spacing = 10.px
            }
        }
    }
}