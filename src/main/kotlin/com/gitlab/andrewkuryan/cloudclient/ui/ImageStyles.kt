package com.gitlab.andrewkuryan.cloudclient.ui

import javafx.geometry.Pos
import tornadofx.Stylesheet
import tornadofx.cssclass
import tornadofx.px

class ImageStyles : Stylesheet() {

    companion object {
        const val controlsHeight = 50

        val imageRoot by cssclass()
        val imageWrapper by cssclass()
        val bottomControls by cssclass()
    }

    init {
        imageRoot {
            prefWidth = MainStyles.winWidth.px
            prefHeight = MainStyles.winHeight.px

            imageWrapper {
                prefHeight = (MainStyles.winHeight -
                        controlsHeight -
                        MainStyles.headerHeight).px
                prefWidth = MainStyles.winWidth.px
                alignment = Pos.CENTER
            }

            bottomControls {
                prefHeight = controlsHeight.px
            }
        }
    }
}