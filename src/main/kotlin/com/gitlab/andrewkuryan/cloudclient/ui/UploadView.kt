package com.gitlab.andrewkuryan.cloudclient.ui

import com.gitlab.andrewkuryan.cloudclient.application.port.UploadModel
import javafx.scene.control.ProgressIndicator
import javafx.scene.control.TextField
import javafx.stage.FileChooser
import kfoenix.jfxbutton
import kfoenix.jfxtextfield
import tornadofx.*

class UploadView : View() {

    private val model: UploadModel by di()

    private lateinit var spinner: ProgressIndicator
    private lateinit var devicePath: TextField
    private lateinit var uploadedName: TextField

    init {
        importStylesheet<UploadStyles>()
    }

    override val root = stackpane {
        addClass(UploadStyles.uploadRoot)
        vbox {
            addClass(FilesStyles.filesRoot)
            hbox {
                addClass(MainStyles.header)
                button("Back") {
                    action {
                        model.close()
                    }
                }
            }
            vbox {
                vbox {
                    label("Choose file:")
                    hbox {
                        devicePath = jfxtextfield {
                            textProperty().addListener { _, _, newValue ->
                                model.devicePath = newValue
                            }
                        }
                        jfxbutton("Choose") {
                            action {
                                chooseFile("Choose File",
                                        arrayOf(FileChooser.ExtensionFilter("image", "*.png", "*.jpg"))
                                ).firstOrNull()?.let {
                                    devicePath.text = it.absolutePath
                                    uploadedName.text = it.name
                                }
                            }
                        }
                    }
                }
                vbox {
                    label("Uploaded name")
                    uploadedName = jfxtextfield {
                        textProperty().addListener { _, _, newValue ->
                            model.serverName = newValue
                        }
                    }
                }
                jfxbutton("Upload") {
                    action {
                        model.upload()
                    }
                }
            }
        }
        spinner = progressindicator {}
    }

    override fun onDock() {
        super.onDock()
        model.setIsLoadingListener {
            spinner.isVisible = it
        }
        spinner.isVisible = model.isLoading
    }

    override fun onUndock() {
        super.onUndock()
        model.setIsLoadingListener {}
    }
}