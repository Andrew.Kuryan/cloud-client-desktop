package com.gitlab.andrewkuryan.cloudclient.ui

import javafx.scene.paint.Color
import tornadofx.Stylesheet
import tornadofx.box
import tornadofx.cssclass
import tornadofx.px

class MainStyles : Stylesheet() {

    companion object {
        const val winWidth = 400
        const val winHeight = 600
        const val headerHeight = 50

        val mainRoot by cssclass()
        val header by cssclass()
        val floatingButton by cssclass()
    }

    init {
        header {
            prefHeight = headerHeight.px
        }

        mainRoot {
            prefWidth = winWidth.px
            prefHeight = winHeight.px
        }

        floatingButton {
            backgroundColor += Color.WHITE
            prefWidth = 50.px
            prefHeight = 50.px
            backgroundRadius += box(25.px)
            fontSize = 18.px
        }
    }
}