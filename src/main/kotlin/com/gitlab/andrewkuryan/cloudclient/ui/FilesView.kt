package com.gitlab.andrewkuryan.cloudclient.ui

import com.gitlab.andrewkuryan.cloudclient.application.port.FilesModel
import com.gitlab.andrewkuryan.cloudclient.entity.resource.DirectoryResource
import com.gitlab.andrewkuryan.cloudclient.entity.resource.FileResource
import com.gitlab.andrewkuryan.cloudclient.entity.resource.ImageResource
import com.jfoenix.controls.JFXButton
import javafx.scene.Parent
import javafx.scene.control.Label
import javafx.scene.control.ListView
import javafx.scene.control.ProgressIndicator
import kfoenix.jfxbutton
import tornadofx.*

class FilesView : View() {

    private val model by di<FilesModel>()

    private lateinit var spinner: ProgressIndicator
    private lateinit var pathLabel: Label
    private lateinit var filesList: ListView<Parent>

    init {
        importStylesheet<FilesStyles>()
    }

    override val root = stackpane {
        addClass(FilesStyles.filesRoot)
        vbox {
            addClass(FilesStyles.filesRoot)
            hbox {
                addClass(MainStyles.header)
                button("Back") {
                    action {
                        model.navigateBack()
                    }
                }
                pathLabel = label()
            }
            filesList = listview {
                addClass(FilesStyles.filesList)
            }
        }
        jfxbutton("+") {
            addClass(MainStyles.floatingButton, FilesStyles.plusButton)
            buttonType = JFXButton.ButtonType.RAISED
            action {
                model.onAddFileClick()
            }
        }
        spinner = progressindicator {}
    }

    override fun onDock() {
        super.onDock()
        model.setIsLoadingListener {
            spinner.isVisible = it
        }
        spinner.isVisible = model.isLoading
        model.setOnCurrentPathChangeListener {
            pathLabel.text = it
        }
        pathLabel.text = model.currentPath
        model.setOnResourcesChangeListener { resources ->
            filesList.items = resources.map {
                when (it) {
                    is DirectoryResource -> DirectoryItem(it, model::onResourceClick)
                    is FileResource -> FileItem(it, model::onResourceClick)
                    else -> FileItem(it as FileResource, model::onResourceClick)
                }
            }.map { it.root }.observable()
        }
        filesList.items = model.resources.map {
            when (it) {
                is DirectoryResource -> DirectoryItem(it, model::onResourceClick)
                is FileResource -> FileItem(it, model::onResourceClick)
                else -> FileItem(it as FileResource, model::onResourceClick)
            }
        }.map { it.root }.observable()
        if (model.resources.size == 0) {
            model.navigateTo("")
        }
    }

    override fun onUndock() {
        super.onUndock()
        model.setIsLoadingListener { }
        model.setOnCurrentPathChangeListener { }
        model.setOnResourcesChangeListener { }
    }
}

class DirectoryItem(directory: DirectoryResource, onClick: (DirectoryResource) -> Unit) : View() {

    override val root = anchorpane {
        addClass(FilesStyles.directoryItem)
        hbox {
            addClass(FilesStyles.directoryContent)
            anchorpaneConstraints {
                topAnchor = 0.0
                bottomAnchor = 0.0
                leftAnchor = 0.0
                rightAnchor = 0.0
            }
            imageview("/images/folder_black_48dp.png") {
                fitWidth = 25.0
                fitHeight = 25.0
            }
            label(directory.name)
        }
        setOnMouseClicked {
            onClick(directory)
        }
    }
}

class FileItem(file: FileResource, onClick: (FileResource) -> Unit) : View() {

    override val root = anchorpane {
        addClass(FilesStyles.filesItem)
        hbox {
            addClass(FilesStyles.filesContent)
            anchorpaneConstraints {
                topAnchor = 0.0
                bottomAnchor = 0.0
                leftAnchor = 0.0
                rightAnchor = 0.0
            }
            imageview(when (file) {
                is ImageResource -> "/images/collections_black_48dp.png"
                else -> "/images/insert_drive_file_black_48dp.png"
            }) {
                fitWidth = 25.0
                fitHeight = 25.0
            }
            label(file.name)
        }
        setOnMouseClicked {
            onClick(file)
        }
    }
}