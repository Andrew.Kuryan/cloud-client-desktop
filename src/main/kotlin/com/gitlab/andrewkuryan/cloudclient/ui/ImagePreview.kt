package com.gitlab.andrewkuryan.cloudclient.ui

import com.gitlab.andrewkuryan.cloudclient.application.port.ImageModel
import javafx.scene.control.ProgressIndicator
import javafx.scene.image.Image
import javafx.scene.image.ImageView
import tornadofx.*
import java.io.ByteArrayInputStream

class ImagePreview : View() {

    private val model by di<ImageModel>()

    private lateinit var imageView: ImageView
    private lateinit var spinner: ProgressIndicator

    init {
        importStylesheet<ImageStyles>()
    }

    override val root = stackpane {
        addClass(ImageStyles.imageRoot)
        vbox {
            hbox {
                addClass(MainStyles.header)
                button("Back") {
                    action {
                        model.close()
                    }
                }
                label()
            }
            addClass(ImageStyles.imageRoot)
            flowpane {
                addClass(ImageStyles.imageWrapper)
                imageView = imageview {
                    fitWidth = MainStyles.winWidth.toDouble()
                    fitHeight = (MainStyles.winHeight -
                            ImageStyles.controlsHeight -
                            MainStyles.headerHeight).toDouble()
                    isPreserveRatio = true
                }
            }
            hbox {
                addClass(ImageStyles.bottomControls)
                button("Prev") {
                    action {
                        model.movePrev()
                        model.getCurrentImageContent(::onLoadFinished)
                    }
                }
                button("Next") {
                    action {
                        model.moveNext()
                        model.getCurrentImageContent(::onLoadFinished)
                    }
                }
            }
        }
        spinner = progressindicator {

        }
    }

    private fun onLoadFinished() {
        imageView.image = Image(ByteArrayInputStream(model.loadedResources[model.currentResourcePos]))
    }

    override fun onDock() {
        super.onDock()
        model.setIsLoadingListener {
            spinner.isVisible = it
        }
        spinner.isVisible = model.isLoading
        model.getCurrentImageContent(::onLoadFinished)
    }

    override fun onUndock() {
        super.onUndock()
        model.setIsLoadingListener {}
    }
}