package com.gitlab.andrewkuryan.cloudclient.ui

import tornadofx.Stylesheet
import tornadofx.cssclass
import tornadofx.px

class UploadStyles : Stylesheet() {

    companion object {
        val uploadRoot by cssclass()
    }

    init {
        uploadRoot {
            prefWidth = MainStyles.winWidth.px
            prefHeight = MainStyles.winHeight.px
        }
    }
}