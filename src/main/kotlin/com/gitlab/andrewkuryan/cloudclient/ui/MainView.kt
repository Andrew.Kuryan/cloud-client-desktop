package com.gitlab.andrewkuryan.cloudclient.ui

import com.gitlab.andrewkuryan.cloudclient.application.CloudClientApplication
import com.gitlab.andrewkuryan.cloudclient.application.port.CloudApp
import com.gitlab.andrewkuryan.cloudclient.application.port.FilesModel
import com.gitlab.andrewkuryan.cloudclient.application.port.ImageModel
import com.gitlab.andrewkuryan.cloudclient.application.port.UploadModel
import com.gitlab.andrewkuryan.cloudclient.entity.smb.SimpleSmbCredentials
import tornadofx.*
import kotlin.reflect.KClass

class MainView : View() {

    private val cloudApp: CloudApp = CloudClientApplication(
            navigateToImage = {
                root.children.clear()
                root.add<ImagePreview>()
            },
            navigateToFiles = {
                root.children.clear()
                root.add<FilesView>()
            },
            navigateToUpload = {
                root.children.clear()
                root.add<UploadView>()
            },
            navigateToUsers = {

            }
    )

    init {
        importStylesheet<MainStyles>()

        FX.dicontainer = object : DIContainer {
            @Suppress("UNCHECKED_CAST")
            override fun <T : Any> getInstance(type: KClass<T>) =
                    when (type) {
                        FilesModel::class -> cloudApp.files as T
                        ImageModel::class -> cloudApp.image as T
                        UploadModel::class -> cloudApp.upload as T
                        else -> throw IllegalArgumentException()
                    }
        }

        cloudApp.servers.connectWithCredentials(SimpleSmbCredentials(
                "localhost/Test"
        )) {
            cloudApp.navigateTo("files")
        }
    }

    override val root = stackpane {
        addClass(MainStyles.mainRoot)
    }
}