package com.gitlab.andrewkuryan.cloudclient

import com.gitlab.andrewkuryan.cloudclient.dataprovider.ResourceNotFoundException
import com.gitlab.andrewkuryan.cloudclient.dataprovider.smd.SMBFileApi
import com.gitlab.andrewkuryan.cloudclient.entity.resource.DirectoryResource
import com.gitlab.andrewkuryan.cloudclient.entity.resource.FileResource
import com.gitlab.andrewkuryan.cloudclient.entity.smb.SmbCredentials
import org.junit.Test
import java.nio.file.Files
import java.nio.file.Paths
import kotlin.test.assertEquals

class SmbFileApiTest {

    private val api = SMBFileApi()
    private val testHost = "127.0.0.1"

    @Test
    fun `should connect to server without credentials`() {
        val result = api.connect("$testHost/Test")
        api.disconnect(result.getOrNull()!!)

        assert(result.isSuccess)
        assertEquals("Guest", result.getOrNull()!!.login)
    }

    @Test
    fun `should return error if couldn't connect to server without credentials`() {
        val result = api.connect("$testHost/SecureTest")
        assert(result.isFailure)
    }

    @Test
    fun `should connect to server with credentials`() {
        val result = api.connectWithCredentials(
                SmbCredentials("$testHost/SecureTest", "ASUS-ANDREW", "smbuser", "12345678")
        )
        api.disconnect(result.getOrNull()!!)

        assert(result.isSuccess)
        assertEquals("smbuser", result.getOrNull()!!.login)
    }

    @Test
    fun `should return error if incorrect password`() {
        val result = api.connectWithCredentials(
                SmbCredentials("$testHost/SecureTest", "ASUS-ANDREW", "smbuser", "1234")
        )
        assert(result.isFailure)
    }

    @Test
    fun `should return an existing file resource`() {
        val userResult = api.connect("$testHost/Test")
        val resourceResult = api.getResource(userResult.getOrNull()!!, "/testdir/innertest.txt", "/")
        api.disconnect(userResult.getOrNull()!!)

        assert(resourceResult.isSuccess)
        assertEquals("/testdir/innertest.txt", resourceResult.getOrNull()!!.path)
        assert(resourceResult.getOrNull()!! is FileResource)
    }

    @Test
    fun `should return an existing directory resource`() {
        val userResult = api.connect("$testHost/Test")
        val resourceResult = api.getResource(userResult.getOrNull()!!, "/testdir", "/")
        api.disconnect(userResult.getOrNull()!!)

        assert(resourceResult.isSuccess)
        assertEquals("/testdir", resourceResult.getOrNull()!!.path)
        assert(resourceResult.getOrNull()!! is DirectoryResource)
    }

    @Test
    fun `should return error if resource not exists`() {
        val userResult = api.connect("$testHost/Test")
        val resourceResult = api.getResource(userResult.getOrNull()!!, "/testdir/1.txt", "/")
        api.disconnect(userResult.getOrNull()!!)

        assert(resourceResult.isFailure)
        assert(resourceResult.exceptionOrNull()!! is ResourceNotFoundException)
    }

    @Test
    fun `should return a directory content`() {
        val userResult = api.connect("$testHost/Test")
        val resourceResult = api.getDirectoryContent(userResult.getOrNull()!!, "/testdir", "/")
        api.disconnect(userResult.getOrNull()!!)

        assert(resourceResult.isSuccess)
        resourceResult.getOrNull()!!.forEach {
            assert(it.path.startsWith("/testdir"))
        }
    }

    @Test
    fun `should upload new file`() {
        val userResult = api.connect("$testHost/Test")
        val uploadResult = api.uploadFile(
                userResult.getOrNull()!!,
                "/testdir/upload.txt",
                Files.readAllBytes(Paths.get("src/test/resources/upload_test.txt"))
        )
        api.disconnect(userResult.getOrNull()!!)

        assertEquals(null, uploadResult)
    }
}