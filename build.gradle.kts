import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    id("org.jetbrains.kotlin.jvm").version("1.3.50")
    application
}

repositories {
    jcenter()
    mavenLocal()
}

val compileKotlin: KotlinCompile by tasks
compileKotlin.kotlinOptions.jvmTarget = "1.8"
compileKotlin.kotlinOptions.freeCompilerArgs = listOf("-Xallow-result-return-type")

val tornadofxVersion = "1.7.17"

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    implementation("no.tornado:tornadofx:$tornadofxVersion")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-javafx:1.3.0-RC")
    implementation("com.appspot.magtech:kfoenix:0.1.4")
    
    implementation("eu.agno3.jcifs:jcifs-ng:2.1.3")

    testImplementation("org.jetbrains.kotlin:kotlin-test")
    testImplementation("org.jetbrains.kotlin:kotlin-test-junit")
}

application {
    mainClassName = "com.gitlab.andrewkuryan.cloudclient.FXApp"
}
